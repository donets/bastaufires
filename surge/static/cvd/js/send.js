// Запрос IP адреса
clientIp = '';
posTop20 = true;
posTop40 = true;
posTop60 = true;
posTop80 = true;

// Запрос IP адреса
$.ajax({
    url:'https://freegeoip.app/json/',
    type:'get',
    dataType:'json',
    // 1async: false,
    success:function(data) {
        clientIp = data.ip;
        clientCountry = data.country_name;
        clientRegion = data.region_name;
    }
});

document.getElementById('body').style.display = 'inherit';
const player = new Plyr('#player', {
});

// Функции для работы с куки
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = encodeURIComponent(cname) + "=" + encodeURIComponent(cvalue) + ";" + expires + ";path=/"/* + ";secure"*/;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

if (getCookie('was_last_day') != "") {
    sessionId = getCookie('sd');
} else {
    setCookie('was_last_day', true);
    sessionId = Date.now() - 1577815200000;     // Установить идентификатор сессии

    maxScrollPercent = 0;
    setCookie('sd', sessionId);                 // Записать идентификатор в куки и хранить сутки
    setCookie('srl', maxScrollPercent, 1);
}


// Отправить данные после загрузки страницы
$(document).ready(function sendOnloadData() {
    var clientWidth = document.documentElement.clientWidth;
    var clientHeight = document.documentElement.clientHeight;
    
    var ua = detect.parse(navigator.userAgent);
    var clientBrowser = (ua.browser.family);
    var clientBrowserVersion = (ua.browser.version);
    var clientPlatform = (ua.os.family);
    var clientPlatformVersion = (ua.os.version);

    var sendFlag = false;

    var xhr = new XMLHttpRequest();
    var url = "https://iqlink.dst-group.kz/api/usage/";
    xhr.withCredentials = true;
    // xhr.open(method, URL, [async, user, password])
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    
    var csrftoken = document.cookie.match(/csrftoken=([\w-]+)/);

    if (csrftoken) {
        console.log('csrf');
        xhr.setRequestHeader("X-CSRF-Token", csrftoken);
    }

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            // console.log(json.platform + ", " + json.width + ", " + 
            //     json.height + ", " + json.browser + ", " + json.ip);
            console.log(json);
        }
    };

    // Проверка всех параметров на изменение перед отправкой, если изменений
    // нет, то не отправлять повторно
    setTimeout(() => {
        if (getCookie('srlChange') == "true" ||  
            window.location.href != getCookie('uri') || 
            clientPlatform != getCookie('plf') || 
            clientBrowser != getCookie('brw') ||
            clientIp != getCookie('adr') ||
            clientWidth != getCookie('wid') ||
            clientHeight != getCookie('hig') ||
            !getCookie('sd')
            ) {
            setCookie('srlChange', false, 1);
            setCookie('uri', window.location.href, 1);
            setCookie('plf', clientPlatform, 1);
            setCookie('brw', clientBrowser, 1);
            setCookie('wid', clientWidth, 1);
            setCookie('hig', clientHeight, 1);
            setCookie('adr', clientIp, 1);

            sendFlag = true;
        } else {
            sendFlag = false;
        }

        var data = JSON.stringify({
            "tm": Date.now(),               // Время отправки
            "sd": getCookie('sd'),          // ID сессии
            "ct": "",
            "uri": window.location.href,    // Текущий url
            "srl": getCookie('srl'),
            "plf": clientPlatform, 
            "brw": clientBrowser, 
            "agn": navigator.userAgent,
            "adr": clientIp,
            "wid": clientWidth, 
            "hig": clientHeight, 
        });
            
        // Отправка JSON
        if (sendFlag)
            xhr.send(data);
    }, 1000);
});

function getPlatform() {
    var userDeviceArray = [
    {device: 'Android', platform: /Android/},
    {device: 'iPhone', platform: /iPhone/},
    {device: 'iPad', platform: /iPad/},
    {device: 'Symbian', platform: /Symbian/},
    {device: 'Windows Phone', platform: /Windows Phone/},
    {device: 'Tablet OS', platform: /Tablet OS/},
    {device: 'Linux', platform: /Linux/},
    {device: 'Windows', platform: /Windows NT/},
    {device: 'Macintosh', platform: /Macintosh/}
    ];

    var user = navigator.userAgent;

    for (var i in userDeviceArray) {
        if (userDeviceArray[i].platform.test(user)) {
            return userDeviceArray[i].device;
        }
    }
    return user;
}

function getBrowser() {
    var userBrowserArray = [
    {browser: 'Mozilla Firefox', navigatorBrowser: /Firefox/},
    {browser: 'Safari', navigatorBrowser: /Safari/},
    {browser: 'Google Chrome', navigatorBrowser: /Chrome/},
    {browser: 'Yandes Browser', navigatorBrowser: /YaBrowser/},
    {browser: 'Opera', navigatorBrowser: /OPR/},
    {browser: 'Konqueror', navigatorBrowser: /Konqueror/},
    {browser: 'Iceweasel', navigatorBrowser: /Iceweasel/},
    {browser: 'SeaMonkey', navigatorBrowser: /SeaMonkey/},
    {browser: 'Microsoft Edge', navigatorBrowser: /Edge/},
    ];

    var user = navigator.userAgent;

    // alert(user);

    for (var i in userBrowserArray) {
        if (userBrowserArray[i].navigatorBrowser.test(user)) {
            return userBrowserArray[i].browser;
        }
    }
}

window.onscroll = function() {
    posTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

    if ((posTop / document.body.clientHeight >= 0.19) && (posTop / document.body.clientHeight <= 0.21)) {
        maxScrollPercent = 20;
        if (posTop20) {
            sendBreakpoint(20);
            if (maxScrollPercent > getCookie('srl')) {
                setCookie('srl', maxScrollPercent, 1);
                setCookie('srlChange', true, 1);
            }
            posTop20 = false;
        }
    }
    if ((posTop / document.body.clientHeight >= 0.39) && (posTop / document.body.clientHeight <= 0.41)) {
        maxScrollPercent = 40;
        if (posTop40) {
            sendBreakpoint(40);
            if (maxScrollPercent > getCookie('srl')) {
                setCookie('srl', maxScrollPercent, 1);
                setCookie('srlChange', true, 1);
            }
            posTop40 = false;
        }
    }
    if ((posTop / document.body.clientHeight >= 0.59) && (posTop / document.body.clientHeight <= 0.61)) {
        maxScrollPercent = 60;
        if (posTop60) {
            sendBreakpoint(60);
            if (maxScrollPercent > getCookie('srl')) {
                setCookie('srl', maxScrollPercent, 1);
                setCookie('srlChange', true, 1);
            }
            posTop60 = false;
        }
    }
    if ((posTop / document.body.clientHeight >= 0.79) && (posTop / document.body.clientHeight <= 0.81)) {
        maxScrollPercent = 80;
        if (posTop80) {
            sendBreakpoint(80);
            if (maxScrollPercent > getCookie('srl')) {
                setCookie('srl', maxScrollPercent, 1);
                setCookie('srlChange', true, 1);
            }
            posTop80 = false;
        }
    }
}

function sendBreakpoint(breakpoint) {
    var xhr = new XMLHttpRequest();
    var url = "https://iqlink.dst-group.kz/api/usage/";
    // xhr.open(method, URL, [async, user, password])
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    // xhr.onreadystatechange = function () {
    //     if (xhr.readyState === 4 && xhr.status === 200) {
    //         var json = JSON.parse(xhr.responseText);
    //         console.log(json.platform + ", " + json.width + ", " + 
    //             json.height + ", " + json.browser + ", " + json.ip);
    //     }
    // };

    var data = JSON.stringify({"sd": getCookie('sd'), "srl": breakpoint});
    
    // Отправка JSON
    xhr.send(data);
}
